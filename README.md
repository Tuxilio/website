# Source code of Tuxilio's personal website ![CI status](https://ci.codeberg.org/api/badges/13212/status.svg)

This is the source code for Tuxilio's Personal Website.

- You can find my website here: <https://tuxilio.codeberg.page/>
- You can find the source code here: <https://codeberg.org/Tuxilio/website/>
- The source code is build on each push using the Codeberg CI to this repository: <https://codeberg.org/Tuxilio/pages/>

## How to build

This is a static website using [Hugo](https://gohugo.io) to be built. So you need Hugo to build this.

You have to clone the submodules first:

    git submodule update --init --recursive

If you want to update, use:

    git pull
    git submodule update --remote --merge

To build, just type this into the console:

    hugo

Then retrieve the result from the directory `public`.
This directory contains all the webpages and data files and can be uploaded to a webserver.

If you want to build the site again, delete `public` first.

## License

- All text is under CC-BY-SA 4.0, unless otherwise noted.
- All code is under GPLv3.0 or later, unless otherwise noted.

The Tuxilio Penguin is from [FreeSVG](https://freesvg.org/penguin-laughing-vector-graphics), licensed under CC0.

Other logos and screenshots might fall under different conditions, see [`content/meta/copying.md`](https://tuxilio.codeberg.page/meta/copying/) for details.
