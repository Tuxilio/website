---
author: "Tuxilio"
title: '{{ replace .File.ContentBaseName "-" " " | title }}'
date: {{ .Date }}
description: '{{ replace .File.ContentBaseName "-" " " | title }}'
draft: true
tags: [
    "100DaysToOffload",
]
toc: true
---
