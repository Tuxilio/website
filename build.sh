hugo

rm -rf /tmp/pages
mkdir /tmp/pages
cp -a public/. /tmp/pages/
cp -a extra-content/. /tmp/pages/
cd /tmp/pages

git config --global init.defaultBranch main
git init
git remote add origin "git@codeberg.org:Tuxilio/pages.git"
git add .
git commit -m "[CI] Build website ($(env TZ=Europe/Berlin date +"%d.%m.%Y %H:%M")) (Manual build) [SKIP CI]"
git push --force origin main
