module github.com/hugo-mods/icons

go 1.22.2

require (
	github.com/hugomods/icons/vendors/simple-icons v1.0.36 // indirect
)
