---
title: "Home"
description: "welcome to my blog"
---

### // About me

I'm **Tuxilio** and this is my space on the internet. <span class="p-note">I like penguins, free software, programming, building things and free video games.</span>

Practically all of my published work is compatible with free software / free culture values. Most of my code can be found on [Codeberg](https://codeberg.org/Tuxilio).
