---
title: "More"
description: "Uses, Favourites and other places to look for content"
---

## Credits for this site

### Tools

- **Hugo** (<https://gohugo.io/>): *an open-source static site generator*.
- **HugoMods icons** (<https://icons.hugomods.com/>): *lightweight Hugo SVG icon module*
- **tuxi-hugo** (<https://codeberg.org/Tuxilio/tuxi-hugo/>): *my own Hugo theme*
    - Please view <https://codeberg.org/Tuxilio/tuxi-hugo/#licenses> for all licenses

### Copying

- Please see [Copying](/meta/copying)

### Hosting

- This blog is hosted on **Codeberg Pages** (<https://codeberg.page/>).

## Stats

- For statistics on this blog, [see /stats](/stats).

## Blogroll

* [Pinguin](https://blog.derpingu.in/)
* [bbbhltz](https://bbbhltz.codeberg.page/) ([RSS](https://bbbhltz.codeberg.page/rss.xml))
* [Kev Quirk](https://kevquirk.com/) ([RSS](https://kevquirk.com/feed.xml))
* [Dominik George](https://dominik-george.de/)

## Badges

[![512 kb club](/img/badges/512-kb-club-green-team.svg)](https://512kb.club/)

## xkcd I like

*Taken from [xkcd.com](https://xkcd.com/), licensed [CC BY-NC 2.5](https://creativecommons.org/licenses/by-nc/2.5/).*
*This means you're free to copy and share these comics (but not to sell them).*

*Not done yet, sorry*

### Other software

- For more information on the software I like to use, [see /software](/software).
