---
author: "Tuxilio"
title: 'Tuxmoji'
date: 2024-04-25T15:28:15+02:00
description: 'Tuxmoji'
tags: [
    "projects",
    "javascript",
    "html",
]
---
I created a free & open source emoji selector named [&#128039; Tuxmoji](https://tuxilio.codeberg.page/Tuxmoji/).

You can [use it here](https://tuxilio.codeberg.page/Tuxmoji/)!

[{{< ico simple-icons codeberg >}} Repo](https://codeberg.org/Tuxilio/Tuxmoji)
