---
author: "Tuxilio"
title: 'Chocoprint'
date: 2024-04-05T12:58:51+02:00
description: 'chocolate 3d printer'
tags: [
    "projects",
    "3d-printing",
]
---

The project chocoprint is a chocolate 3D printer I made with two friends.

Our goal is to develop a low-cost chocolate 3D printer that everyone can replicate. The project started as a hobby project in 2023.

Find more out and maybe (re)build your own printer at [{{< ico simple-icons codeberg >}} the project repo](https://chocoprint.codeberg.page)!.

![Chocoprint printer](chocoprint_ft_printer.png)
