---
author: "Tuxilio"
title: 'Forgejo Stats'
date: 2024-04-05T17:40:00+02:00
description: 'User statistics for Forgejo / Gitea'
tags: [
    "projects",
    "forgejo",
    "ci",
    "python",
]
---
<div align="center">
<iframe src="https://tuxilio.codeberg.page/forgejo-buttons/buttons/?url=https://codeberg.org/&user=tuxilio&repo=forgejo-stats&size=large&count=true&dark=false&ico=img/star-dark.svg&type=star" style="all: initial; background-color: transparent; width: 170px; height: 30px;" frameborder="0" scrolling="0" width="170px" height="30px" allowtransparency="true" title="forgejo:buttons"></iframe>
<iframe src="https://tuxilio.codeberg.page/forgejo-buttons/buttons/?url=https://codeberg.org/&user=tuxilio&repo=forgejo-stats&size=large&count=true&dark=false&ico=img/eye-dark.svg&type=watch" style="all: initial; background-color: transparent; width: 170px; height: 30px;" frameborder="0" scrolling="0" width="170px" height="30px" allowtransparency="true" title="forgejo:buttons"></iframe>
<iframe src="https://tuxilio.codeberg.page/forgejo-buttons/buttons/?url=https://codeberg.org/&user=tuxilio&repo=forgejo-stats&size=large&count=true&dark=false&ico=img/issue-opened-dark.svg&type=issue" style="all: initial; background-color: transparent; width: 170px; height: 30px;" frameborder="0" scrolling="0" width="170px" height="30px" allowtransparency="true" title="forgejo:buttons"></iframe>
</div>


This project generates statistics about your Forgejo or Gitea profile using Python and (if you want) automatically using the Woodpecker CI.



<iframe src="https://tuxilio.codeberg.page/forgejo-buttons/buttons/?url=https://codeberg.org/&user=tuxilio&repo=forgejo-stats&size=large&count=true&dark=false&ico=img/repo-template-dark.svg&type=template" style="all: initial; background-color: transparent; width: 170px; height: 30px;" frameborder="0" scrolling="0" width="170px" height="30px" allowtransparency="true" title="forgejo:buttons"></iframe>


Find the [{{< ico simple-icons codeberg >}} source code on Codeberg](https://codeberg.org/Tuxilio/forgejo-stats/)!



<a href="https://codeberg.org/Tuxilio/forgejo-stats">
<img src="https://codeberg.org/Tuxilio/forgejo-stats/raw/branch/main/generated/overview.svg#gh-dark-mode-only"/>
<img src="https://codeberg.org/Tuxilio/forgejo-stats/raw/branch/main/generated/languages.svg#gh-dark-mode-only"/>
<br>
<img src="https://codeberg.org/Tuxilio/forgejo-stats/raw/branch/main/generated/overview.svg#gh-light-mode-only"/>
<img src="https://codeberg.org/Tuxilio/forgejo-stats/raw/branch/main/generated/languages.svg#gh-light-mode-only"/>
</a>

