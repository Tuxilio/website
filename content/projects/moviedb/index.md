---
author: "Tuxilio"
title: 'MovieDB'
date: 2024-04-05T17:00:00+02:00
description: 'Movie database to manage your movies'
tags: [
    "projects",
    "python",
    "sqlite",
]
---
This is a project to manage your movies.

The development is currently stall, but you can still use it.

Find the [{{< ico simple-icons codeberg >}} source code here](https://codeberg.org/Tuxilio/MovieDB)!
