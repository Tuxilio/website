---
author: "Tuxilio"
title: 'Forgejo Buttons'
date: 2024-04-05T17:40:00+02:00
description: 'forgejo:buttons'
tags: [
    "projects",
    "forgejo",
    "javascript",
    "html",
]
---
<div align="center">
<iframe src="https://tuxilio.codeberg.page/forgejo-buttons/buttons/?url=https://codeberg.org&user=tuxilio&repo=forgejo-buttons&size=large&count=true&dark=false&ico=img/star-dark.svg&type=star" style="all: initial; background-color: transparent; width: 170px; height: 30px;" frameborder="0" scrolling="0" width="170px" height="30px" allowtransparency="true" title="forgejo:buttons"></iframe>
<iframe src="https://tuxilio.codeberg.page/forgejo-buttons/buttons/?url=https://codeberg.org&user=tuxilio&repo=forgejo-buttons&size=large&count=true&dark=false&ico=img/eye-dark.svg&type=watch" style="all: initial; background-color: transparent; width: 170px; height: 30px;" frameborder="0" scrolling="0" width="170px" height="30px" allowtransparency="true" title="forgejo:buttons"></iframe>
<iframe src="https://tuxilio.codeberg.page/forgejo-buttons/buttons/?url=https://codeberg.org&user=tuxilio&repo=forgejo-buttons&size=large&count=true&dark=false&ico=img/issue-opened-dark.svg&type=issue" style="all: initial; background-color: transparent; width: 170px; height: 30px;" frameborder="0" scrolling="0" width="170px" height="30px" allowtransparency="true" title="forgejo:buttons"></iframe>
</div>

This project generates buttons for repositorys on Forgejo or Gitea.

You can [use it here](https://tuxilio.codeberg.page/forgejo-buttons/)!

[{{< ico simple-icons codeberg >}} Source code](https://codeberg.org/Tuxilio/forgejo-buttons)

<article>
<h2><a href="https://tuxilio.codeberg.page/forgejo-buttons/">Latest release</a></h2>
<p>
<a href="https://codeberg.org/Tuxilio/forgejo-buttons/releases/tag/v1.1">v1.2</a> - HREF fixes
<br>
<a href="/blog/2024/06/07/forgejobuttons-v1.2-release/">Blog post - forgejo:buttons v1.2 release 🎉</a>
</p>
</article>
