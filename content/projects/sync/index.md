---
author: "Tuxilio"
title: 'Sync'
date: 2024-04-05T17:00:00+02:00
description: 'Sync repos from GitHub to a Forgejo / Gitea instance'
tags: [
    "projects",
    "shell",
    "ci",
    "forgejo",
]
---
A project to sync your issues, pull requests, ... from GitHub to Codeberg using GitHub actions. WiP.

[{{< ico simple-icons codeberg >}} Codeberg repo](codeberg.org/Tuxilio/sync-test)
