---
author: "Tuxilio"
title: 'Markdown Cheatsheet'
date: 2024-05-30T09:54:32+02:00
description: 'Markdown Cheatsheet'
tags: [
    "markdown",
]
toc: true
---
## Headers

# H1
```markdown
# H1
```

## H2
```markdown
## H2
```

### H3
```markdown
### H3
```

#### H4
```markdown
#### H4
```

##### H5
```markdown
##### H5
```

###### H6
```markdown
###### H6
```

---

## Emphasis

*Italic*
```markdown
*Italic* or _Italic_
```

**Bold**
```markdown
**Bold** or __Bold__
```

***Bold and Italic***
```markdown
***Bold and Italic*** or ___Bold and Italic___
```

~~Strikethrough~~
```markdown
~~Strikethrough~~
```

---

## Lists

### Unordered List

- Item 1
- Item 2
  - Subitem 2.1
  - Subitem 2.2
- Item 3

```markdown
- Item 1
- Item 2
  - Subitem 2.1
  - Subitem 2.2
- Item 3
```

### Ordered List

1. Item 1
2. Item 2
   1. Subitem 2.1
   2. Subitem 2.2
3. Item 3

```markdown
1. Item 1
2. Item 2
   1. Subitem 2.1
   2. Subitem 2.2
3. Item 3
```

### Task List

- [ ] Task 1
- [x] Task 2 (completed)
- [ ] Task 3

```markdown
- [ ] Task 1
- [x] Task 2 (completed)
- [ ] Task 3
```

---

## Links

### Inline Links

[Link Text](http://example.com)
```markdown
[Link Text](http://example.com)
```

### Reference Links

[Link Text][1]
```markdown
[Link Text][1]
```
[1]: http://example.com
```markdown
[1]: http://example.com
```

### Automatic Links

<http://example.com>
```markdown
<http://example.com>
```

---

## Images

### Inline Images

![Alt Text](/favicon.ico)
```markdown
![Alt Text](/favicon.ico)
```

### Reference Images

![Alt Text][1]
```markdown
![Alt Text][1]
```
[1]: /favicon.ico
```markdown
[1]: /favicon.ico
```

---

## Code

### Inline Code

`inline code`
```markdown
`inline code`
```

### Code Block

```
code block
```

```markdown
```
code block
```
```

### Indented Code Block

    Indented code block (by 4 spaces or 1 tab)

```markdown
    Indented code block (by 4 spaces or 1 tab)
```

### Fenced Code Block with Syntax Highlighting

```javascript
function example() {
    console.log("Hello, world!");
}
```

```markdown
```javascript
function example() {
    console.log("Hello, world!");
}
```
```

---

## Blockquotes

> This is a blockquote.
>
> This is another paragraph in the blockquote.

```markdown
> This is a blockquote.
>
> This is another paragraph in the blockquote.
```

## Horizontal Rule

---
```markdown
---
```

***
```markdown
***
```

___
```markdown
___
```

---

## Tables

| Header 1 | Header 2 |
|----------|----------|
| Cell 1   | Cell 2   |
| Cell 3   | Cell 4   |

```markdown
| Header 1 | Header 2 |
|----------|----------|
| Cell 1   | Cell 2   |
| Cell 3   | Cell 4   |
```

### Alignment in Tables

| Left Aligned | Center Aligned | Right Aligned |
|:-------------|:--------------:|--------------:|
| Cell 1       | Cell 2         | Cell 3        |
| Cell 4       | Cell 5         | Cell 6        |

```markdown
| Left Aligned | Center Aligned | Right Aligned |
|:-------------|:--------------:|--------------:|
| Cell 1       | Cell 2         | Cell 3        |
| Cell 4       | Cell 5         | Cell 6        |
```

---

## Footnotes

Here is a footnote reference[^1].

[^1]: This is the footnote.

```markdown
Here is a footnote reference[^1].

[^1]: This is the footnote.
```

---

## Definitions

Term 1
: Definition 1

Term 2
: Definition 2

```markdown
Term 1
: Definition 1

Term 2
: Definition 2
```

---

## Emojis

😀 ❤️ 👍

```markdown
:smile: :heart: :+1:
```

---

## Details and Summary

<details>
<summary>Click to expand!</summary>

This is hidden content that can be toggled by clicking the summary.
</details>

```markdown
<details>
<summary>Click to expand!</summary>

This is hidden content that can be toggled by clicking the summary.
</details>
```

---

## Escaping Characters

\*literal asterisks\*

```markdown
\*literal asterisks\*
```

---

This cheatsheet covers the basics of Markdown syntax. You can include this in your Hugo blog to help others (and yourself) quickly reference Markdown commands.

