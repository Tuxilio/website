---
author: "Tuxilio"
title: 'Free Software Facts'
date: 2024-05-29T16:24:48+02:00
description: 'About Free Software'
tags: [
    "foss",
]
toc: true
---

## tl;dr

The [GNU article about free software](https://www.gnu.org/philosophy/free-sw) has a good conclusion about free software:

> “Free software” means software that respects users' freedom and community. Roughly, it means that **the users have the freedom to run, copy, distribute, study, change and improve the software**. Thus, “free software” is a matter of liberty, not price. To understand the concept, you should think of “free” as in “free speech,” not as in “free beer.” We sometimes call it “libre software,” borrowing the French or Spanish word for “free” as in freedom, to show we do not mean the software is gratis.
> You may have paid money to get copies of a free program, or you may have obtained copies at no charge. But regardless of how you got your copies, you always have the freedom to copy and change the software, even to [sell copies](https://www.gnu.org/philosophy/selling.html).


## Introduction
Public awareness of the concept of free software remains limited. Although some people might have encountered the term before, many do not fully understand it. Even developers sometimes misunderstand it.

## Free software respects user’s essential freedoms
According to the [Free Software Definition](https://www.gnu.org/philosophy/free-sw), any given software is a free software if and only if all of these 4 freedoms are guaranteed:

> - The freedom to run the program as you wish, for any purpose (freedom 0).
> - The freedom to study how the program works, and change it so it does your computing as you wish (freedom 1). Access to the source code is a precondition for this.
> - The freedom to redistribute copies so you can help others (freedom 2).
> - The freedom to distribute copies of your modified versions to others (freedom 3). By doing this you can give the whole community a chance to benefit from your changes. Access to the source code is a precondition for this.

These are also known as the “four freedoms”.

## Free software is not the same as freeware

With “freeware”, people usually mean software that costs nothing. Under this definition, a freeware might or might not be free software as well. However, other people define “freeware” as any proprietary software that costs nothing. Under this definition, a freeware can never be free software.

## Free software can be commercial
“Free software” does not mean “noncommercial”. Free software is about the user having the four freedoms (see above). The price of the software is simply a separate question. A free software may or may not cost nothing. It depends on whoever is redistributing the software.

What you can not do with free software is to declare a monopoly on it and forbid anyone else from selling copies.

In practice, most free software is gratis.

## Open source vs free software

“Open source” is something different: it has a very different philosophy based on different values. Its practical definition is different too, but nearly all open source programs are in fact free. Richard Stallmann wrote a nice article about it: [Why Open Source misses the point of free software](https://www.gnu.org/philosophy/open-source-misses-the-point.html)

## Read more

I recommend you to read the following articles:

- [GNU's article about free software](https://www.gnu.org/philosophy/free-sw)
- [Free software facts](https://wuzzy.codeberg.page/essays/fsfacts/) by wuzzy

## Footnotes
This is heavily inspired by [Wuzzy's free software page (CC0)](https://wuzzy.codeberg.page/essays/fsfacts/) and [GNU's definition of free software](https://www.gnu.org/philosophy/free-sw)
