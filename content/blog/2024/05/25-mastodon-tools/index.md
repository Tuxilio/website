---
author: "Tuxilio"
title: 'Mastodon Tools'
date: 2024-05-25T13:58:19+02:00
description: 'Nice Mastodon Tools'
draft: false
tags: [
    "mastodon",
]
toc: false
---

I found some nice [Mastodon](https://joinmastodon.org/) tools. Now I want to show these. It's just a short text :)

### [Mastopoet](https://mastopoet.raikas.dev/) - Take awesome picutures of Mastodon posts

![Mastopoet image of Tuxilio's post](mastopoet-tuxilio.jpg)

![Mastopoet Website](mastopoet-website.jpg)

- [Website](https://mastopoet.raikas.dev/)
- [Repo](https://github.com/raikasdev/mastopoet)

### [Top Mastodon Posts](https://www.topmastodonposts.com/) - Display top posts by an user

- [Website](https://www.topmastodonposts.com/)

### [MastodonTootFollower](https://mastodon-toot-follower.privacy1st.de/) - Follow a specific post

- [Website](https://mastodon-toot-follower.privacy1st.de/)

### More

- <https://github.com/hueyy/awesome-mastodon?tab=readme-ov-file>
