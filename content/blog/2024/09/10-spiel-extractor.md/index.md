---
author: "Tuxilio"
title: 'SPIEL Exhibitor Extractor'
date: 2024-09-10T13:52:40+02:00
description: 'Spiel 2024 Essen Exhibitor Extractor -> CSV'
tags: [
]
toc: true
---
[SPIEL](https://www.spiel-essen.de/) in Essen is the world's largest public fair for board games. There is an app, hall plans etc... but not a list of all exhibitors to download.

This simple python script helps to list all exhibitors with stand numbers and save them in a CSV file.

```python
import requests
import csv

# URL to JSON file
url = 'https://maps.eyeled-services.de/de/spiel24/exhibitors?columns=%5B%22ID%22%2C%22NAME%22%2C%22ADRESSE%22%2C%22LAND%22%2C%22LOGO%22%2C%22PLZ%22%2C%22STADT%22%2C%22WEB%22%2C%22EMAIL%22%2C%22INFO%22%2C%22TELEFON%22%2C%22S_ORDER%22%2C%22STAND%22%2C%22HALLE%22%5D'

# Send request
response = requests.get(url)
data = response.json()

# Write to CSV
with open('spiel2024-exhibitors.csv', mode='w', newline='', encoding='utf-8') as file:
    writer = csv.writer(file)
    # write CSV header
    writer.writerow(['Name des Verlags', 'Standnummer'])

    # extract name stand number
    for exhibitor in data.get('exhibitors', []):
        name = exhibitor.get('NAME', 'N/A')
        standnummer = exhibitor.get('STAND', 'N/A')
        # write data to csv
        writer.writerow([name, standnummer])

print("Wrote to 'spiel2024-exhibitors.csv'.")
```
