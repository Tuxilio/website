---
author: "Tuxilio"
title: 'forgejo:buttons v1.0.0 release 🎉'
date: 2024-04-14T14:10:52+02:00
description: 'forgejo:buttons v1.0.0 done'
tags: [
    "javascript",
    "projects",
    "html",
    "forgejo",
]
---

After a lot of work, [forgejo:buttons](https://tuxilio.codeberg.page/forgejo-buttons/) is in version 1.0.0 usable 🎉!

You can now use buttons like Star, Fork, Watch and so on but also create own buttons with custom counter APIs and an own text.

You can embbed the buttons using an iframe to your website. As they're using Javascript, you cannot use it in Markdown. check out <https://shields.io> for this!

If you like the project, give me a star ⭐!
