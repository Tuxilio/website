---
author: "Tuxilio"
title: 'Use Woodpecker CI to build HUGO your website'
date: 2024-04-16T15:09:00+02:00
description: 'This tutorial describes how to use Woodpecker CI to automatically build your website'
tags: [
    "ci",
    "hugo",
    "tutorial",
]
toc: true
---

[Woodpecker CI](https://woodpecker-ci.org/) is a simple and powerful CI/CD engine. In this tutorial I'll show you how you can use it to automatically rebuild and publish your website on each push to a git repo.

In this tutorial I'll describe how to do this using [Codeberg pages](https://codeberg.page/) and the [Codeberg CI](https://ci.codeberg.org/), but you can do it with any other Forge as well. Both is hosted by [Codeberg](https://codeberg.org/), a free git hosting platform.

On Codeberg, you have to request CI access so just fill out the form as described [here](https://docs.codeberg.org/ci/#using-codeberg's-instance-of-woodpecker-ci).

## Basics

You need two repos: one where the page source is located and one where the built page is stored. I have simply called the repository with the source code `website`. The other repository (if you are using Codeberg pages) should be called `pages` as per [this](https://docs.codeberg.org/codeberg-pages/) guide.

## Getting started

### .woodpecker.yaml

Now simply create a file named `.woodpecker.yaml`. This file contains all information for the CI.

A basic file could look like this:

```yaml
steps:
  - name: test
    image: debian
    commands:
      - echo "Hello world!"
  - name: a-test-step
    image: debian
    commands:
      - echo "a test step"
```

Learn more about syntax at the [Woodpecker Docs](https://woodpecker-ci.org/docs/intro)!

### Activating the repository

Now navigate to the CI page and add the repository by simply clicking the `Add repository` button. Choose your repository and make a first test run!

This should print `Hello world` in the step `test` and `a test step` in the step `a-test-step`.

## Creating the workflow

### Basic workflow

```yaml
steps:
  - name: install & build
    image: alpine
    when:
      - event: manual
      - event: push
    commands:
      - apk add --no-cache --repository=https://dl-cdn.alpinelinux.org/alpine/edge/community hugo
      - hugo version
      - hugo

  - name: commit
    image: alpine
    secrets: [token]
    when:
      - event: manual
      - event: push
    commands:
      - apk add git
      - git config --global user.name "[CI] Website builder"
      - git config --global user.email "tuxilio@noreply.codeberg.org"
      - mkdir pages
      - cp -a public/. pages/
      - cd pages
      - git config --global init.defaultBranch main
      - git init
      - git remote add origin "https://"$${TOKEN}"@codeberg.org/tuxilio/pages.git"
      - git add .
      - git commit -m "[CI] Build website ($(env TZ=Europe/Berlin date +"%d.%m.%Y %H:%M")) [SKIP CI]"
      - git push --force origin main
```

### Adding secrets

Inside the CI, select your repository and navigate to the settings page by clicking onto the gear in the top right corner. Select `secrets` and add one named `token` with  a token you create. This will be used when pushing to the repo. Select the option `push` and `manual` so the token could be used in your workflow.

### Explanaition

OK, what is this script doing?

Every workflow starts with a simple `steps`. And because of this is an YAML file, the line should end with an `:`.

#### Build step

Now start with our first step.

We named it `install & build`.

The image is `alpine`, standing for Alpine Linux.

We then define that this step should be executed when either the repo is pushed or when we execute the workflow manually:
```yaml
    when:
      - event: manual
      - event: push
```
This is because, for example, it should not be executed when someone creates an issue.

Now the main workflow starts:
```yaml
    commands:
      - apk add --no-cache --repository=https://dl-cdn.alpinelinux.org/alpine/edge/community hugo
      - hugo version
      - hugo
```
- `apk add [...] hugo` - install `hugo` so we can run it
- `hugo version` - just print the jugo version if somethings wents wrong
- `hugo` - build the website!

And this step is done!

#### Commit step

This step is a little longer, but it's just as easy to understand!

It is named `commit`, runs on `alpine` and on events `push` and `manual`.

Just install `git` using `apk add git`

Now we set the username to `[CI] Website builder` and as the email your email. You can choose whatever you want, but everything is posted as you anyway.
```yaml
      - git config --global user.name "[CI] Website builder"
      - git config --global user.email "tuxilio@noreply.codeberg.org"
```

Now we create a folder called `pages` and copy the code genertaed in the `build` step from `public/`:
```yaml
      - mkdir pages
      - cp -a public/. pages/
      - cd pages
```

Now we set the default branch to `main` (or if you have another default branch, choose another name) and initialize the repo:
```yaml
      - git config --global init.defaultBranch main
      - git init
```

At this point we add the remote. Here the token from secrets will be used. You have to change the repo link to your username, otherwise it would be pushed to my repo which won't work. Then we add the files using `git add .` and commit these. The commit message is set to `[CI] Build website ($(env TZ=Europe/Berlin date +"%d.%m.%Y %H:%M")) [SKIP CI]`. This adds the time and date to the commit message, so it should look like this: `[CI] Build website (16.04.2024 10:55) [SKIP CI]`.
```yaml
      - git remote add origin "https://"$${TOKEN}"@codeberg.org/tuxilio/pages.git"
      - git add .
      - git commit -m "[CI] Build website ($(env TZ=Europe/Berlin date +"%d.%m.%Y %H:%M")) [SKIP CI]"
```

Now, as a last step, push the changes:
```yaml
      - git push --force origin main
```
The force is needed because we overwrite the commit history so the repo doesn't get huge.

## End

You're done 🎉!

This is simply but powerful. I hope I could help someone with this and you're now able to build your HUGO site automatically :)

<p class="notice">
<b>Related articles:</b>
<a href="/blog/use-woodpecker-ci-to-add-extra-content-to-built-website/">Use Woodpecker CI to add additional content to your built website</a>
</p>
