---
author: "Tuxilio"
title: 'Fuiz translation contribution'
date: 2024-04-17T11:09:00+02:00
description: 'I contributed to Fuiz, a free quiz platform'
tags: [
    "foss",
    "alternative",
]
---

[Fuiz](https://fuiz.us/) us a free and privacy-focused alternative to Kahoot. I contributed to it and added german translation a time ago.

I just found Fuiz this year and I hope more people will change from Kahoot. Another alternative to Kahoot is [ClassQuiz](https://classquiz.de/).
