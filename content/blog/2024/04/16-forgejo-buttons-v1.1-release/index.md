---
author: "Tuxilio"
title: 'forgejo:buttons v1.1.0 release 🎉'
date: 2024-04-16T13:30:53+02:00
description: 'forgejo:buttons v1.1 with style improvements'
tags: [
    "javascript",
    "projects",
    "html",
    "forgejo",
]
---

forgejo:buttons is now in version 1.1! The generated iframe code for embedding the button now resets the CSS for this iframe, so that the background of the button adapts to the website even with simple.css.

(My website, for example, uses simple.css, which did not work before)

In addition, the stylesheet of the forgejo:buttons website was changed from min.css to simple.css. Now the website looks a little better and also has a dark mode 🎉

[Use it!](https://tuxilio.codeberg.page/forgejo-buttons/)

## Demo

<div style="background: #33cc33;">
<iframe src="https://tuxilio.codeberg.page/forgejo-buttons/buttons/?url=https://codeberg.org&user=tuxilio&repo=forgejo-buttons&size=large&count=true&dark=false&ico=img/star-dark.svg&type=star" style="all: initial; background-color: transparent; width: 170px; height: 30px;" frameborder="0" scrolling="0" width="170px" height="30px" allowtransparency="true" title="forgejo:buttons"></iframe>
<iframe src="https://tuxilio.codeberg.page/forgejo-buttons/buttons/?url=https://codeberg.org&user=tuxilio&repo=forgejo-buttons&size=large&count=true&dark=true&ico=img/star-light.svg&type=star" style="all: initial; background-color: transparent; width: 170px; height: 30px;" frameborder="0" scrolling="0" width="170px" height="30px" allowtransparency="true" title="forgejo:buttons"></iframe>
</div>

<div style="background: #ff9933;">
<iframe src="https://tuxilio.codeberg.page/forgejo-buttons/buttons/?url=https://codeberg.org&user=tuxilio&repo=forgejo-buttons&size=large&count=true&dark=false&ico=img/star-dark.svg&type=star" style="all: initial; background-color: transparent; width: 170px; height: 30px;" frameborder="0" scrolling="0" width="170px" height="30px" allowtransparency="true" title="forgejo:buttons"></iframe>
<iframe src="https://tuxilio.codeberg.page/forgejo-buttons/buttons/?url=https://codeberg.org&user=tuxilio&repo=forgejo-buttons&size=large&count=true&dark=true&ico=img/star-light.svg&type=star" style="all: initial; background-color: transparent; width: 170px; height: 30px;" frameborder="0" scrolling="0" width="170px" height="30px" allowtransparency="true" title="forgejo:buttons"></iframe>
</div>

<div style="background: #3399ff;">
<iframe src="https://tuxilio.codeberg.page/forgejo-buttons/buttons/?url=https://codeberg.org&user=tuxilio&repo=forgejo-buttons&size=large&count=true&dark=false&ico=img/star-dark.svg&type=star" style="all: initial; background-color: transparent; width: 170px; height: 30px;" frameborder="0" scrolling="0" width="170px" height="30px" allowtransparency="true" title="forgejo:buttons"></iframe>
<iframe src="https://tuxilio.codeberg.page/forgejo-buttons/buttons/?url=https://codeberg.org&user=tuxilio&repo=forgejo-buttons&size=large&count=true&dark=true&ico=img/star-light.svg&type=star" style="all: initial; background-color: transparent; width: 170px; height: 30px;" frameborder="0" scrolling="0" width="170px" height="30px" allowtransparency="true" title="forgejo:buttons"></iframe>
</div>

<div style="background: #000000;">
<iframe src="https://tuxilio.codeberg.page/forgejo-buttons/buttons/?url=https://codeberg.org&user=tuxilio&repo=forgejo-buttons&size=large&count=true&dark=false&ico=img/star-dark.svg&type=star" style="all: initial; background-color: transparent; width: 170px; height: 30px;" frameborder="0" scrolling="0" width="170px" height="30px" allowtransparency="true" title="forgejo:buttons"></iframe>
<iframe src="https://tuxilio.codeberg.page/forgejo-buttons/buttons/?url=https://codeberg.org&user=tuxilio&repo=forgejo-buttons&size=large&count=true&dark=true&ico=img/star-light.svg&type=star" style="all: initial; background-color: transparent; width: 170px; height: 30px;" frameborder="0" scrolling="0" width="170px" height="30px" allowtransparency="true" title="forgejo:buttons"></iframe>
</div>

<div style="background: #ffffff;">
<iframe src="https://tuxilio.codeberg.page/forgejo-buttons/buttons/?url=https://codeberg.org&user=tuxilio&repo=forgejo-buttons&size=large&count=true&dark=false&ico=img/star-dark.svg&type=star" style="all: initial; background-color: transparent; width: 170px; height: 30px;" frameborder="0" scrolling="0" width="170px" height="30px" allowtransparency="true" title="forgejo:buttons"></iframe>
<iframe src="https://tuxilio.codeberg.page/forgejo-buttons/buttons/?url=https://codeberg.org&user=tuxilio&repo=forgejo-buttons&size=large&count=true&dark=true&ico=img/star-light.svg&type=star" style="all: initial; background-color: transparent; width: 170px; height: 30px;" frameborder="0" scrolling="0" width="170px" height="30px" allowtransparency="true" title="forgejo:buttons"></iframe>
</div>

If you like the project, give me a star ⭐!
