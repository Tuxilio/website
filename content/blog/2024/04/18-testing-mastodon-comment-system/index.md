---
author: "Tuxilio"
title: 'Testing Mastodon Comment System'
date: 2024-04-18T07:57:16+02:00
description: 'Testing Mastodon Comment System'
tags: [
    "mastodon",
]
comments:
  host: toot.teckids.org
  username: tuxilio
  id: 112290674602230410
---
This is just a test to test the Mastodon comment system. The most code is by [Carl Schwan](https://carlschwan.eu/2020/12/29/adding-comments-to-your-static-blog-with-mastodon/) and [Tony Cheneau](https://amnesiak.org/post/2021/01/30/hugo-blog-with-mastodon-comments-extended/).

You can learn how this is implemented [here](/blog/2024/04/19/mastodon-comment-system-for-your-website/)

If you want to leave a comment, visit the page on mastodon and reply. It will be shown on this page.
