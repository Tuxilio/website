---
author: "Tuxilio"
title: 'My First Post'
date: 2024-04-05T13:00:52+02:00
description: 'My First Post'
tags: [
    "markdown",
]
---
## Introduction

This is **bold** text, and this is *emphasized* text.

## Made with

Made with [Hugo](https://gohugo.io) and [hugo-simple](https://themes.gohugo.io/themes/hugo-simple/) theme. *Hugo Simple* is based on [Simple.css](https://simplecss.org/) and
[Hugo Bear](https://github.com/janraasch/hugo-bearblog/).
