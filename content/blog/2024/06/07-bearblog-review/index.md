---
author: "Tuxilio"
title: 'Bearblog review'
date: 2024-06-07T16:34:42+02:00
description: 'Bearblog review'
tags: [
    "review",
]
toc: true
---
## Introduction

I am trying out [Bearblog](https://bearblog.dev/).

The project says about itself:

> A privacy-first, no-nonsense, super-fast blogging platform
> No trackers, no javascript, no stylesheets. Just your words.
> --- https://bearblog.dev/

and further:

> - Looks great on **any** device
> - Tiny (~2.7kb), optimized, and awesome pages
> - No trackers, ads, or scripts
> - Seconds to sign up
> - Connect your custom domain
> - Free themes
> - RSS & Atom feeds
> - Built to last forever[*](https://herman.bearblog.dev/building-software-to-last-forever/)
> --- https://bearblog.dev/

This is a great idea I think. One of the best things is the small website.

![Bear Blog main page](bearblog-main.png)

## Getting started

### Creating an account

You need to create an account first.
Therefore, you need to provide an (valid) email address and (obviously) a password.

When you sign up, you can choos a free `*.bearblog.dev` subdomain, a title of your site and first homepage text. Don't worry, you can change all this later.

As a last step, click the link in the confirmation email and you're done!

### Writing your first content

Log in the first time now!
You will see your dashboard, showing your blogs and some links with account settings.
We will look to these later, but first choose your blog you created before.

You now will see your post main menu.

You can edit the text in the box (supporting [Markdown](/blog/2024/05/30/markdown-cheatsheet/)).
Click the `Publish` button to publish your text, and then view it!
(Either by clicking on the `View` button, or go to the previously created subdomain)

You have now created your first home page!

## Creating navigation links and posts

Now that you have your homepage set up, it's time to add some navigation links and create your first post.

On your post main menu, you'll see a section called *Nav*. Here, you can add links to other pages on your blog, such as an *About* page or a *Contact* page. Simply enter the URL and the link text, and click *Add Link*.

To create a new post, navigate to *Posts* and click on the *New Post* button. You'll be taken to a page where you can enter the title and body of your post. Bear Blog supports Markdown, so you can format your text using headers, bold, italics, links, and more. When you're happy with your post, click *Publish* and it will be live on your blog!

## Customizing your blog

Bear Blog offers a range of free themes to choose from, so you can customize the look and feel of your blog. To change your theme, go to the *Themes* section of your dashboard. You can preview the different themes and choose the one that suits your style.

And you can also use your own CSS!

## Conclusion

Bear Blog is a simple and privacy-focused blogging platform that is easy to use and highly customizable. With its support for Markdown, free themes, and custom domains, you can create a beautiful and professional-looking blog without sacrificing your privacy or loading your site with unnecessary scripts and trackers.
