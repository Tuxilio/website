---
author: "Tuxilio"
title: 'forgejo:buttons v1.2 release 🎉'
date: 2024-06-07T13:30:53+02:00
description: 'forgejo:buttons v1.2 with fixed button links'
tags: [
    "javascript",
    "projects",
    "html",
    "forgejo",
]
---
<div align="center">
<iframe src="https://tuxilio.codeberg.page/forgejo-buttons/buttons/?url=https://codeberg.org&user=tuxilio&repo=forgejo-buttons&size=large&count=true&dark=false&ico=img/star-dark.svg&type=star" style="all: initial; background-color: transparent; width: 170px; height: 30px;" frameborder="0" scrolling="0" width="170px" height="30px" allowtransparency="true" title="forgejo:buttons"></iframe>
<iframe src="https://tuxilio.codeberg.page/forgejo-buttons/buttons/?url=https://codeberg.org&user=tuxilio&repo=forgejo-buttons&size=large&count=true&dark=false&ico=img/eye-dark.svg&type=watch" style="all: initial; background-color: transparent; width: 170px; height: 30px;" frameborder="0" scrolling="0" width="170px" height="30px" allowtransparency="true" title="forgejo:buttons"></iframe>
<iframe src="https://tuxilio.codeberg.page/forgejo-buttons/buttons/?url=https://codeberg.org&user=tuxilio&repo=forgejo-buttons&size=large&count=true&dark=false&ico=img/issue-opened-dark.svg&type=issue" style="all: initial; background-color: transparent; width: 170px; height: 30px;" frameborder="0" scrolling="0" width="170px" height="30px" allowtransparency="true" title="forgejo:buttons"></iframe>
</div>

forgejo:buttons is now in version 1.2! The button links were broken and are fixed now.

[Use it!](https://tuxilio.codeberg.page/forgejo-buttons/)

If you like the project, give me a star ⭐!
