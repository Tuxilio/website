---
author: "Tuxilio"
title: 'Copying'
date: 2024-04-05T13:26:01+02:00
---

This website is released under the {{< license "CC BY-SA 4.0" >}}, as far as is legally possible. Exceptions and further credits are listed below.

## Tools

Please see [/more#tools](/more/#tools).

## Text

All written text is written by Tuxilio (except for quotations, obviously) and is released under the {{< license "CC BY-SA 4.0" >}}.

## Images

The following conditions apply for images:
- The Tuxilio Penguin: Image from [FreeSVG](https://freesvg.org/penguin-laughing-vector-graphics), licensed under CC0.

## Screenshots

- Matopoet screenshot: Website of [Mastopoet](https://mastopoet.raikas.dev/), licensed under [MIT](https://github.com/raikasdev/mastopoet)

## Code

All code in the [website repo](https://codeberg.org/Tuxilio/website) and the [theme repo](https://codeberg.org/Tuxilio/tuxi-hugo/) is licensed under {{< license "GPLv3" >}}, unless otherwise noted. Please see [/more#tools](/more/#tools)
