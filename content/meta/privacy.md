---
author: "Tuxilio"
title: 'Privacy'
date: 2024-04-05T13:35:44+02:00
---

### tl;dr
* [No tracking](https://themarkup.org/blacklight?location=eu&device=desktop&force=false&url=tuxilio.codeberg.page) of personal identifiable information trough the author ([Tuxilio](/))
* Server operator might collect personal identifiable information ([Codeberg's privacy policy](https://codeberg.org/Codeberg/org/src/branch/main/PrivacyPolicy.md))

### Full

Tuxilio, the author and publisher of this website does not collect or store any personal identifiable information.
However, the server operator (who is not Tuxilio) might collect personal identifiable information (such as the IP address). You can find Codeberg's privacy policy here: <https://codeberg.org/Codeberg/org/src/branch/main/PrivacyPolicy.md>

If you contact me, treat it as if we were in a public chatroom. Please be aware our communication can be read and tracked by many people along the way. This is the Internet, after all. While I won’t track you, I can’t promise that other people won’t track you. So please do not send highly-sensitive information to me.
