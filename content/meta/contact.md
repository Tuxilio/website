---
author: "Tuxilio"
title: 'Contact'
date: 2024-04-05T13:35:35+02:00
---

This page contains my contact information. I speak German and English.

# E-mail

<!-- comments to protect against spam bots -->
ma<!--gufejvwwhh-->il (at) <!-- sdfjsdhfkjypcs -->tuxi<!--ruvdgjnbgdgu-->l (dot) io</p>

# Matrix

[@tuxilio:tchncs.de](https://matrix.to/#/@tuxilio:tchncs.de)

# XMPP

[tuxilio@jabber.ccc.de](xmpp:tuxilio@jabber.ccc.de)

# Codeberg issue

You can open an issue on [Codeberg](https://codeberg.org/Tuxilio/website/issues) (publicy visible).

# Mastodon

[@Tuxilio@toot.teckids.org](https://toot.teckids.org/@Tuxilio) (publicy visible).
