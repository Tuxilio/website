# pages

This is the built code for Tuxilio's personal website.

**Here is not the place to edit anything.** As already said, this is the *built* code.

**All data is erased on rebuilt!** All files are built by Hugo, then the files in the [`static`](https://codeberg.org/Tuxilio/website/src/branch/main/static) folder are copied into the pages repository.

- You can find my website here: <https://tuxilio.codeberg.page/>
- You can find the source code here: <https://codeberg.org/Tuxilio/website/>
- The source code is build on each push using the Codeberg CI to this repository: <https://codeberg.org/Tuxilio/pages/>

## License

- All text is under CC-BY-SA 4.0, unless otherwise noted.
- All code is under GPLv3.0 or later, unless otherwise noted.

The Tuxilio Penguin is from [FreeSVG](https://freesvg.org/penguin-laughing-vector-graphics), licensed under CC0.

Other logos and screenshots might fall under different conditions, see [`content/meta/copying.md`](https://tuxilio.codeberg.page/meta/copying/) for details.
